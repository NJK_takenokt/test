cmake_minimum_required(VERSION 3.13.1)

file( GLOB_RECURSE
  COMP_SOURCES
  ${CMAKE_CURRENT_LIST_DIR}/*.cpp
)

file( GLOB_RECURSE
  COMP_HEADERS
  ${CMAKE_CURRENT_LIST_DIR}/*.hpp
)

list_subdirectories(COMP_INCL_PATHS ${CMAKE_CURRENT_LIST_DIR} FALSE TRUE)


message("incl paths ${COMP_INCL_PATHS}")