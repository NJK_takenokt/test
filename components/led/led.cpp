#include "led.hpp"

#include <drivers/gpio.h>

#include <string>
namespace peripherals {

/* The devicetree node identifier for the "led0" alias. */
#define LED0_NODE DT_ALIAS(led0)
#define LED1_NODE DT_ALIAS(led1)
#define LED2_NODE DT_ALIAS(led2)
/*
 * Devicetree helper macro which gets the 'flags' cell from a 'gpios'
 * property, or returns 0 if the property has no 'flags' cell.
 */

#define FLAGS_OR_ZERO(node)                                                    \
  COND_CODE_1(                                                                 \
    DT_PHA_HAS_CELL(node, gpios, flags), (DT_GPIO_FLAGS(node, gpios)), (0))

const Led::GpioConfig Led::DEVICE_CONFIGS[Led::DEVICE_COUNT]{
  {
#if DT_NODE_HAS_STATUS(LED0_NODE, okay)
    .gpio_dev_name = DT_GPIO_LABEL(LED0_NODE, gpios),
    .gpio_pin_name = DT_LABEL(LED0_NODE),
    .gpio_pin = DT_GPIO_PIN(LED0_NODE, gpios),
    .gpio_flags = GPIO_OUTPUT | FLAGS_OR_ZERO(LED0_NODE),

#endif
  },
  {
#if DT_NODE_HAS_STATUS(LED1_NODE, okay)
    .gpio_dev_name = DT_GPIO_LABEL(LED1_NODE, gpios),
    .gpio_pin_name = DT_LABEL(LED1_NODE),
    .gpio_pin = DT_GPIO_PIN(LED1_NODE, gpios),
    .gpio_flags = GPIO_OUTPUT | FLAGS_OR_ZERO(LED1_NODE),
#endif
  },
  {
#if DT_NODE_HAS_STATUS(LED2_NODE, okay)
    .gpio_dev_name = DT_GPIO_LABEL(LED2_NODE, gpios),
    .gpio_pin_name = DT_LABEL(LED2_NODE),
    .gpio_pin = DT_GPIO_PIN(LED2_NODE, gpios),
    .gpio_flags = GPIO_OUTPUT | FLAGS_OR_ZERO(LED2_NODE),

#endif
  },
};

Led::Led(Led::Id led_id)
  : on_{ false }
  , device_{ nullptr }
{
  std::size_t raw_led_id{ static_cast<std::size_t>(led_id) };
  const GpioConfig& device_config{ DEVICE_CONFIGS[raw_led_id] };

  device_ = device_get_binding(device_config.gpio_dev_name);
  __ASSERT_NO_MSG(device_ != NULL); // what if assert is disabled

  gpio_pin_configure(device_,
                     device_config.gpio_pin,
                     device_config.gpio_flags | GPIO_OUTPUT_ACTIVE);

  if (device_ != nullptr) {
    pin_ = device_config.gpio_pin;
    gpio_pin_set(device_, pin_, 0);
  }
}

void
Led::turn_on()
{
  if (!on_) {
    gpio_pin_set(device_, pin_, 1);
    on_ = true;
  }
}

void
Led::turn_off()
{
  if (on_) {
    gpio_pin_set(device_, pin_, 0);
    on_ = false;
  }
}

bool
Led::toggle()
{
  if (on_) {
    turn_off();
  } else {
    turn_on();
  }

  return on_;
}

} // namespace peripherals
