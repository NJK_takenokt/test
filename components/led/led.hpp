#ifndef LED_HPP
#define LED_HPP

#include <cstdint>
#include <drivers/gpio.h>
#include <string>

struct device;

namespace peripherals {

class Led
{
public:
  enum class Id
  {
    Led0,
    Led1,
    Led2
  };

  explicit Led(Id led_id);

  void turn_on();
  void turn_off();

  bool toggle();

private:
  struct GpioConfig
  {
    const char* gpio_dev_name;
    const char* gpio_pin_name;
    gpio_pin_t gpio_pin;
    gpio_flags_t gpio_flags;
  };

  static constexpr std::size_t DEVICE_COUNT{ 3 };
  static const GpioConfig DEVICE_CONFIGS[Led::DEVICE_COUNT];

  bool on_;
  const struct device* device_;
  std::uint32_t pin_;
};

} // namespace peripherals

#endif // LED_HPP
