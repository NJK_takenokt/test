# README #

This is the base repo for all Daikin embedded platforms

# Getting started

After cloning this repository in order to get started to contribute to the project, the project dependencies and the tooling must be installed. 

This will be installed automatically in a folder `_deps` the first time you will build the Application.

## Requirements

In order to contribute to the project, you need the following Software to be installed locally:
* CMake v3.20+ (c.f. https://cmake.org/ )
* Ninja (https://github.com/ninja-build/ninja/releases)
* JLink Software Package v7+ (c.f. https://www.segger.com/downloads/jlink/)
* Python3 (https://www.python.org/downloads/)

## Set Gitlab authentication token

In order to download all dependencies, CMake needs an environment variable `GITLAB_AUTH_TOKEN` containing the authentication to https://gitlab.com with the following format
```
<LOGIN>:<PERSONAL ACCESS TOKEN or PASSWORD>@
```

Rather than using your PASSWORD, you can create an PERSONAL ACCESS TOKEN on https://gitlab.com/-/profile/personal_access_tokens

Finally, you can keep this environment variable in your bash by adding this line to at the end of your $HOME/.bashrc on Linux
```
export GITLAB_AUTH_TOKEN="<LOGIN>:<PERSONAL ACCESS TOKEN or PASSWORD>@"
```

On Windows you just add this environment variable to your System Environment Variables.

## Building the Application

Building the `Application` code is done by triggering the `build-application` task defined in `.vscode/tasks.json`.

This can be achieved with `Terminal > Run task ...` and select `build-application` or by using the `Task Explorer` view if the extension is installed.

Otherwise, this can be achieved by executing the following commands at the root of the project on Linux (For windows just use the Run Task in VSCode):

```
export board=nucleo_f767zi
export build_dir_zephyr=_build/$board
export application_dir=Application
cmake -DVERBOSE=ON -G Ninja -S $application_dir -D BOARD=$board -B $build_dir_zephyr && ninja -C $build_dir_zephyr
```

This action will execute the following:
1. Create a `deps` folder if it doesn't exist and install any dependency
1. Create a `_build` folder to generate the Makefiles and then build the Application code

These both folders are ignored by Git since it must not be part of the repository.

# Dependencies

This projects depends on the following projects:
* Zephyr Project (c.f. https://docs.zephyrproject.org/latest/index.html)

## Create Application
The source code must be place into the components directory.
