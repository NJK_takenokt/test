## Where to find svd files

* For STM32
   1. goto https://www.st.com/en/microcontrollers-microprocessors/stm32-32-bit-arm-cortex-mcus.html
   2. Select a the left side the __correct controller__
   3. Select '__Resources__' and then '__HW Model,...__
   4. Select __System View Description__
