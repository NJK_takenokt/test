macro(cmake_force_rebuild)
    set( TEST_FILE "CMakeLists.txt")
    add_custom_target(dummy_target ALL
        DEPENDS
        always_rebuild
    )

    add_custom_command(
        OUTPUT always_rebuild
        COMMAND ${CMAKE_COMMAND} -E touch  ${PROJECT_SOURCE_DIR}/${TEST_FILE}
        COMMENT "Creating ====> ${PROJECT_SOURCE_DIR}/${TEST_FILE}"
    )
endmacro()