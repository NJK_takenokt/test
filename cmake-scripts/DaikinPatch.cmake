macro( cmake_apply_patches)

  # step one  create list of patches
  file(GLOB PATCHES "${CMAKE_SOURCE_DIR}/../daikin-platform/zephyr-patches/*.patch")
  # step 2 apply patches

  if (PATCHES)
    message(STATUS "Patches: ${PATCHES}")
    foreach(PATCH ${PATCHES})
      message(STATUS "Applying ${PATCH}")
      execute_process(
        COMMAND patch -p1 --forward --ignore-whitespace
        WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/../zephyr-source"
        INPUT_FILE "${PATCH}"
        OUTPUT_VARIABLE OUTPUT
        RESULT_VARIABLE RESULT)
      if (RESULT EQUAL 0)
        message(STATUS "Patch applied: ${PATCH}")
      else()
        # Unfortunately although patch will recognise that a patch is already
        # applied it will still return an error.
        execute_process(
          COMMAND patch -p1 -R --dry-run
          WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/../zephyr-source"
          INPUT_FILE "${PATCH}"
          OUTPUT_VARIABLE OUTPUT
          RESULT_VARIABLE RESULT2)
        if (RESULT2 EQUAL 0)
          # message(STATUS "Patch was already applied: ${PATCH}")
        else()
          message(FATAL_ERROR "Error applying patch ${PATCH}")
        endif()
      endif()
    endforeach(PATCH)
  endif()
endmacro()