# Scans the current directory and returns a list of subdirectories.
# Third parameter is 1 if you want relative paths returned.
# Usage: list_subdirectories(the_list_is_returned_here C:/cwd 1)
macro(list_subdirectories retval curdir return_relative return_recursive)
  if(${return_recursive})
    file(GLOB_RECURSE  sub-dir LIST_DIRECTORIES true  RELATIVE ${curdir} ${curdir}/*)
  else()
    file(GLOB sub-dir RELATIVE ${curdir} ${curdir}/*)
  endif()
  set(list_of_dirs "")
  foreach(dir ${sub-dir})
    if(IS_DIRECTORY ${curdir}/${dir})
      if (${return_relative})
        # set(list_of_dirs ${list_of_dirs} ${dir})
        LIST(APPEND list_of_dirs ${dir})
      else()
        # set(list_of_dirs ${list_of_dirs} ${curdir}/${dir})
        LIST(APPEND list_of_dirs ${curdir}/${dir})
      endif()
    endif()
  endforeach()
  set(${retval} ${list_of_dirs})
endmacro()