#!/bin/bash

# This script aims to create in the local working tree a merge result of source branch into target branch.
# If there are no merge conflicts, the script commits the merge result and output the resulted commit id in a file.
# If there are merge conflicts, the script cleans-up the working tree and exit with failure.

# Display usage of this script
usage() {
  echo "usage: $(basename $0) --source=branch_name --target=branch_name [--result=file] [--help]"
  echo "   ";
  echo "  --source=branch_name : the git branch name to merge into target ref before building";
  echo "  --target=branch_name : the git branch name to build after merge of the source ref";
  echo "  --result=file        : the file to write with merge result commit id";
  echo "  --help               : this message";
}

# Parse input parameters and assign to variables or show usage
parse_args() {
  if [ "$#" -lt "2" ]; then
      echo ""
      echo "[ERROR] Missing parameter."
      echo ""
      usage; exit 1;
  fi

  while [ "$1" != "" ]; do
    case "$1" in
      --source=* ) source=${1:9} ;;
      --target=* ) target=${1:9} ;;
      --result=* ) result=${1:9} ;;
      --help  ) usage; exit 1;;
      * )       usage; exit 1;;
    esac
    shift
  done

  if [ "$result" = "" ]; then
    result="MERGE_RESULT_COMMIT_ID"
  fi
}

# Make sure the local copy of remote repository is up-to-date
fetch_origin() {
  echo " - Fetch remote repository"
  echo "   >>> git fetch origin"
  echo ""
  git fetch origin
  echo ""
  echo "   <<<"
}

# Make sure working tree contains target branch content
prepare_target() {
  echo " - Prepare target branch and print last commit"
  echo "   >>> git checkout $target && git reset --hard origin/$target && git log --oneline -1"
  echo ""  
  git checkout $target && git reset --hard origin/$target && git log --oneline -1
  echo ""  
  echo "   <<<"
  target_ref=$(git rev-parse --abbrev-ref HEAD)
  target_sha=$(git rev-parse --short=8 HEAD)
  echo "   * Current branch:   $target_ref"
  echo "   * Current HEAD SHA: $target_sha"  
  echo ""
}

# Merge source branch into prepared target branch
merge() {
  echo " - Merge source [$source] into target [$target]"
  echo "   * Check local git config"
  GIT_CONFIG_USER_EMAIL=$(git config user.email)
  if [ "$GIT_CONFIG_USER_EMAIL" = "" ]; then
    echo "      - Set missing local user.email to create_merge_result@local";
    git config user.email "create_merge_result@local";
  else
    echo "      - Found local user.email: $GIT_CONFIG_USER_EMAIL";
  fi
  GIT_CONFIG_USER_NAME=$(git config user.name)
  if [ "$GIT_CONFIG_USER_NAME" = "" ]; then
    echo "      - Set missing local user.name to create_merge_result";
    git config user.name "create_merge_result";
  else
    echo "      - Found local user.name: $GIT_CONFIG_USER_NAME";
  fi  
  echo "   * Merge "
  echo "   >>> git merge -v --stat --no-ff --no-edit -m \"Merge branch [$source] into [$target]\" origin/$source"
  echo ""  
  git merge -v --stat --no-ff --no-edit -m "Merge branch [$source] into [$target]" origin/$source
  MERGE_RESULT=$?
  echo ""  
  echo "   <<<"  
  current_ref=$(git rev-parse --abbrev-ref HEAD)
  current_sha=$(git rev-parse --short=8 HEAD)
  echo "   * New commits added on target:"
  echo "   >>> git log --oneline --boundary $target_sha.."
  echo ""  
  git log --oneline --boundary $target_sha..
  echo ""  
  echo "   <<<"
  echo "   * Current branch:   $current_ref"
  echo "   * Current HEAD SHA: $current_sha"  

  if [ "$MERGE_RESULT" = "0" ]; then
    echo "";
    echo "   => MERGE DONE ! Writing $current_sha in [$result].";
    echo -n $current_sha > $result
  else
    echo "";
    echo "   => MERGE ERROR ! Clean-up and Exit with failure !";
    echo "   >>> git merge --abort"
    echo ""  
    git merge --abort
    echo ""  
    echo "   <<<"
    exit 1;
  fi
}

run() {

  echo "-------------------------------------------------------------------"
  echo " $(basename $0) $@ "
  echo "-------------------------------------------------------------------"

  parse_args "$@"

  fetch_origin

  prepare_target

  merge

}

run "$@"
