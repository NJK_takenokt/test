## How to create patches

In case there is an issue in the used zephyr source tree, a patch can be made as follows:

* enter the zephyr_source directory
* `git diff {path/filename} > {filename.patch}`
* copy {filename.patch} to this directory

The patch will be applied in the build process by default