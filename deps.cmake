#####################################################################""
# VARIABLES
IF("$ENV{DEPS_PATH}" STREQUAL "")
    # If no DEPS_PATH is found in the ENV, define here
    set(DEPS_PATH ${CMAKE_CURRENT_LIST_DIR}/_deps)
ELSE()
    # If DEPS_PATH is found in the ENV, reuse it 
    set(DEPS_PATH $ENV{DEPS_PATH})
ENDIF()
message("-- Dependencies are stored in [${DEPS_PATH}]")

set(GCCARM_VERSION "gcc-arm-none-eabi-10-2020-q4-major")
set(GCCARM_PATH ${DEPS_PATH}/gccarm)
set(ZEPHYR_PATH ${DEPS_PATH}/zephyr)
set(GOOGLETEST_VERSION "release-1.10.0")
set(GOOGLETEST_PATH ${DEPS_PATH}/googletest)
# Check Python3 is installed
message("-- Check Python is installed")
find_package(Python3 COMPONENTS Interpreter REQUIRED)

#####################################################################""
# INSTALL DEPENDENCIES IF NEEDED

function(pydeps)
    # Upgrade Pip if needed
    message("-- Check if Python Pip must be upgraded")
    execute_process(COMMAND ${Python3_EXECUTABLE} -m pip install --upgrade pip)
    # Install Python3 Packages
    message("-- Install Python3 Packages into home folder if needed")
    execute_process(COMMAND ${Python3_EXECUTABLE} -m pip install -r ${CMAKE_CURRENT_FUNCTION_LIST_DIR}/requirements.txt)
endfunction()

function(gccarmdeps)
    # GCC ARM
    IF(NOT EXISTS ${GCCARM_PATH})
        message("-- Install GCC ARM Dependency in [${GCCARM_PATH}]")
        include(FetchContent)
        IF (WIN32)
            FetchContent_Declare(arm-none-eabi
                URL https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-rm/10-2020q4/${GCCARM_VERSION}-win32.zip
                SOURCE_DIR ${GCCARM_PATH}
            )
        ELSE()
            FetchContent_Declare(arm-none-eabi
                URL https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-rm/10-2020q4/${GCCARM_VERSION}-x86_64-linux.tar.bz2
                SOURCE_DIR ${GCCARM_PATH}
            )
        ENDIF()
        FetchContent_GetProperties(arm-none-eabi)
        IF(NOT arm-none-eabi_POPULATED)
            FetchContent_Populate(arm-none-eabi)
        ENDIF()
    ELSE()
        message("-- GCC ARM Dependency found in [${GCCARM_PATH}]")
    ENDIF()
    set(ENV{GNUARMEMB_TOOLCHAIN_PATH} ${GCCARM_PATH})
endfunction()

# Zephyr
function(zephyrdeps)
    IF(NOT EXISTS ${ZEPHYR_PATH})
        message("-- Install Zephyr Dependency into [${ZEPHYR_PATH}]")
        execute_process(COMMAND ${Python3_EXECUTABLE} -m pip install west pyelftools>=0.26)
		file(MAKE_DIRECTORY ${DEPS_PATH}/.tmp)
        # The zephyr_path/.. is needed because using a local manifest file
        # West will always put the .west folder 1 level below.
		execute_process(COMMAND ${Python3_EXECUTABLE} -m west init -l --mf ${CMAKE_CURRENT_FUNCTION_LIST_DIR}/west.yml WORKING_DIRECTORY ${DEPS_PATH}/.tmp COMMAND_ECHO STDOUT)
		file(REMOVE_RECURSE ${DEPS_PATH}/.tmp)
    ENDIF()
        message("-- Update Zephyr Dependency found in [${ZEPHYR_PATH}]")
        execute_process(COMMAND ${Python3_EXECUTABLE} -m west update WORKING_DIRECTORY ${DEPS_PATH} COMMAND_ECHO STDOUT)
    set(ENV{ZEPHYR_BASE} ${ZEPHYR_PATH})
endfunction()

IF(${IS_QC_BUILD})
	set(ENV{ZEPHYR_TOOLCHAIN_VARIANT} "llvm")
ELSE()
	set(ENV{ZEPHYR_TOOLCHAIN_VARIANT} "gnuarmemb")
ENDIF()

function(gtestdeps)
    # Google test 
    IF(NOT EXISTS ${GOOGLETEST_PATH})
        message("-- Install Googletest Dependency into [${GOOGLETEST_PATH}]")
        execute_process(COMMAND git clone https://github.com/google/googletest.git ${GOOGLETEST_PATH})
    ENDIF()
    message("-- Check Googletest Dependency into [${GOOGLETEST_PATH}]")
    execute_process(COMMAND git checkout ${GOOGLETEST_VERSION} WORKING_DIRECTORY ${GOOGLETEST_PATH})
endfunction()

function(getbuilddeps)
  pydeps()
  gccarmdeps()
  zephyrdeps()
endfunction()

function(getutdeps)
  pydeps()
  zephyrdeps()
  gtestdeps()
endfunction()