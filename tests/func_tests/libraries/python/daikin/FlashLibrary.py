import os
import platform
import re
from subprocess import run

from pylink import JLink, JLinkResetStrategyCortexM3, library

from . import robot_logger as logger


class FlashLibrary:
    """Library allowing to flash on target using JLink"""

    ROBOT_LIBRARY_SCOPE = "GLOBAL"

    DEVICE_RE = re.compile("Bus ([0-9]{3}) Device ([0-9]{3}).*")
    CONSUMER_RE = re.compile(r"\S*\s*([0-9]+)\s.*")

    def __init__(self, device_name, jlink_serial=None, erase_flash_banks=False, cpu_type=None):
        self.device_name = device_name
        self.jlink_serial = jlink_serial
        self.erase_flash_banks = erase_flash_banks
        self.cpu_type = cpu_type
        if self.jlink_serial:
            logger.info(f"Using specific JLink [{self.jlink_serial}] with device [{self.device_name}]")
        else:
            logger.info(f"Using default JLink with device [{self.device_name}]")

        # Bug on pylink, getting 32 bit version not 64 bit
        if "aarch64" in platform.machine():
            self.jlink = JLink(log=logger.info, lib=library.Library(dllpath="/opt/SEGGER/JLink/libjlinkarm.so"))
        else:
            self.jlink = JLink(log=logger.info)

    def flash_hex(self, hex_path) -> None:
        """
        Flashes the hex file located in hex_path
        """
        logger.info(f"Flashing file [{hex_path}]")

        try:
            self._connect()
            self._reset()
            self._halt()

            # Flash image
            self.jlink.flash_file(path=hex_path, addr=0x0)

            # Reset MCU, close connection
            self._reset()
            self._restart()
        except Exception as e:
            # Log error and consumers
            logger.error(e)
            self._list_consumers()
            # Raise same Exception
            raise e
        finally:
            self.jlink.close()

    def _connect(self) -> None:
        self.jlink.open(serial_no=self.jlink_serial)
        self.jlink.connect(self.device_name, verbose=True)
        self.jlink.power_on()

    def _reset(self, delay: int = 1000) -> None:
        if self.jlink.target_connected():
            if self.cpu_type == "CortexM3":
                self.jlink.set_reset_strategy(JLinkResetStrategyCortexM3.RESETPIN)
            nbBytes = self.jlink.reset(delay, True)
            logger.info(f"{nbBytes} read while resetting")
        else:
            raise Exception(f"While resetting, target device [{self.device_name}] is not connected")

    def _halt(self) -> None:
        if self.jlink.halt():
            logger.info("CPU core halted")
        else:
            raise Exception("CPU core could not be halted")

    def _erase(self) -> None:
        if self.erase_flash_banks:
            self.jlink.exec_command("EnableEraseAllFlashBanks")
        nbBytes = self.jlink.erase()
        logger.info(f"{nbBytes} have been erased")

    def _restart(self) -> None:
        if self.jlink.restart():
            logger.info("CPU core restarted")
        else:
            raise Exception("CPU core could not be restarted")

    def _list_consumers(self) -> None:
        result = run(["lsusb"], capture_output=True, check=True)
        stdout = result.stdout.decode("utf-8")
        devices = [dev for dev in stdout.split("\n") if "J-Link" in dev]
        for device in devices:
            m = self.DEVICE_RE.match(device)

            if m:
                logger.info(f"Found device: {device}")
                self._list_device_consumers(m.group(1), m.group(2))

    def _list_device_consumers(self, bus_id: str, device_id: str) -> None:
        result = run(["lsof", f"/dev/bus/usb/{bus_id}/{device_id}"], capture_output=True, check=False)
        if result.returncode == 1 and not result.stderr and not result.stdout:
            return

        stdout = result.stdout.decode("utf-8")
        for entry in stdout.split("\n"):
            m = self.CONSUMER_RE.match(entry)
            if m:
                pid = os.getpid()
                if pid == int(m.group(1)):
                    logger.info(f"  consumer (this is us): {entry}")
                else:
                    logger.info(f"  consumer: {entry}")
