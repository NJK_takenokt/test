import inspect
import os

from robot.api import logger


def get_stack_info():
    """Get Stack info

    Returns:
        str: Stack info
    """
    calling_stack_info = inspect.stack()[3]
    path = os.path.splitext(os.path.basename(calling_stack_info.filename))[0]
    return f"[{path}.{calling_stack_info.function}]"


def write(msg: str, level: str = "INFO", html: bool = False):
    """Write message

    Args:
        msg (str): message
        level (str, optional): Level (TRACE,DEBUG,INFO,WARN,ERROR). Defaults to "INFO".
        html (bool, optional): [description]. Defaults to False.
    """
    msg = "[{}] {} {}".format(level, get_stack_info(), msg)
    logger.write(msg, level, html)


def trace(msg: str, html: bool = False):
    """Write message with ``TRACE`` level."""
    write(msg, "TRACE", html)


def debug(msg: str, html: bool = False):
    """Writes the message to the log file using the ``DEBUG`` level."""
    write(msg, "DEBUG", html)


def info(msg: str, html: bool = False, also_console: bool = False):
    """Writes the message to the log file using the ``INFO`` level.

    If ``also_console`` argument is set to ``True``, the message is
    written both to the log file and to the console.
    """
    write(msg, "INFO", html)
    if also_console:
        console(msg)


def warn(msg: str, html=False):
    """Writes the message to the log file using the ``WARN`` level."""
    write(msg, "WARN", html)


def error(msg: str, html: bool = False):
    """Writes the message to the log file using the ``ERROR`` level.

    New in Robot Framework 2.9.
    """
    write(msg, "ERROR", html)


def console(msg: str, newline: bool = True, stream: str = "stdout"):
    """Writes the message to the console.

    If the ``newline`` argument is ``True``, a newline character is
    automatically added to the message.

    By default the message is written to the standard output stream.
    Using the standard error stream is possibly by giving the ``stream``
    argument value ``'stderr'``.
    """
    logger.console(msg, newline, stream)
