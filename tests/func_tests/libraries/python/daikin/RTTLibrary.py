import logging
import threading
import time

import pylink
from pylink.errors import JLinkException
from robot.libraries.BuiltIn import BuiltIn

from . import robot_logger as logger


class RTTLibrary:
    """Library giving basic access to JLink RTT behaviour"""

    ROBOT_LIBRARY_SCOPE = "GLOBAL"

    def __init__(self, device: str, jlink_serial=None):
        """Initializes and RTTLibrary object"""
        self._device = device
        self._jlink_serial = jlink_serial
        self._jlink = None
        self._rtt_thread = None
        self._rtt_thread_halt_evt = None
        self._rtt_check = False
        self._rtt_check_evt = None
        self._rtt_uap_stop_evt = None
        self._rtt_thread_pause_evt = None
        self._action_list = []
        fh = logging.FileHandler(
            "{}/rtt.log".format(BuiltIn().replace_variables("${OUTPUTDIR}")),
            encoding="utf-8",
        )
        fh.setLevel(logging.DEBUG)
        formatter = logging.Formatter("%(asctime)s | %(message)s", "%H:%M:%S")
        fh.setFormatter(formatter)
        self.log = logging.getLogger("RTTViewer")
        self.log.setLevel(logging.DEBUG)
        self.log.propagate = False
        self.log.addHandler(fh)

    def __del__(self):
        self.rtt_stop()

    def rtt_start(self):
        """Starts logging the contents of the rtt buffers"""
        # Start logger
        if not self._jlink:
            self._jlink = pylink.JLink()
            self._jlink.disable_dialog_boxes()
            self._jlink.open(serial_no=self._jlink_serial)
            self._jlink.set_tif(pylink.enums.JLinkInterfaces.SWD)
            self._connect_and_start_jlink()

        if not self._rtt_thread_halt_evt:
            self._rtt_thread_halt_evt = threading.Event()

        if not self._rtt_thread_pause_evt:
            self._rtt_thread_pause_evt = threading.Event()

        if not self._rtt_check_evt:
            self._rtt_check_evt = threading.Event()

        if not self._rtt_thread or not self._rtt_thread.is_alive():
            self._rtt_thread = threading.Thread(target=self._rtt_read)
            self._rtt_thread.start()
            logger.debug(f"[RTT]: read thread created, running: {self._rtt_thread.is_alive()}")

    def rtt_stop(self):
        """Stops logging the contents of the rtt buffers"""
        if self._rtt_thread and self._rtt_thread.is_alive():
            logger.debug("stopping thread")
            time.sleep(0.5)
            self._rtt_thread_halt_evt.set()
            self._rtt_thread.join()
            logger.debug("read thread stopped")

            self._reset_variables()

    def rtt_pause(self):
        """Pauses the logger to read from the pipe"""
        if self._rtt_thread_pause_evt:
            self._rtt_thread_pause_evt.set()

    def rtt_play(self):
        """Starts the logger to read from the pipe again"""
        if self._rtt_thread_pause_evt:
            self._rtt_thread_pause_evt.clear()

    def rtt_add_check(self, msg: str):
        """Add RTT check for a specific message

        Args:
            msg (str): Message to check in RTT logs
        """
        self._rtt_thread_halt_evt.clear()
        self._action_list.append(
            (
                lambda line: "{}".format(msg) in line,
                self._send_rtt_check_evt,
            )
        )

    def rtt_check_found(self) -> bool:
        """Indicate if message was found

        Returns:
            bool: True if rtt message was found
        """
        return self._rtt_check_evt.is_set()

    def _connect_and_start_jlink(self):
        self._jlink.connect(self._device)
        self._jlink.rtt_start(None)

    def _rtt_read(self):
        logger.debug("start logger thread reading loop")
        while True:
            if self._jlink.connected():
                try:
                    if not self._rtt_thread_pause_evt.isSet():
                        terminal_bytes = self._jlink.rtt_read(0, 1024)
                        self._parse_terminal_bytes(terminal_bytes)
                        if self._rtt_thread_halt_evt.wait(timeout=0.1):
                            logger.debug("stopping logger thread")
                            break
                    else:
                        self._rtt_thread_pause_evt.wait(0.1)
                except JLinkException as j:
                    logger.error(f"Error occurred while trying to read from JLink: {j}")
            else:
                self._connect_and_start_jlink()
                time.sleep(0.2)

    def _parse_terminal_bytes(self, terminal_bytes):
        if terminal_bytes:
            terminal_bytes = b"".join(map(lambda b: b.to_bytes(1, "little"), terminal_bytes))
            try:
                terminal_str = terminal_bytes.decode("utf-8")
            except UnicodeDecodeError:
                terminal_str = str(terminal_bytes)
                terminal_str = terminal_str.strip("'b").rstrip("'").replace("\\n", "\n")

            for line in terminal_str.splitlines():
                for cond_func, process_func in self._action_list:
                    if cond_func(line):
                        process_func()
                        break
                self.log.info(f"{line}")  # noqa

    def _reset_variables(self):
        if self._jlink:
            self._jlink.close()
            self._jlink = None

        self._rtt_thread = None
        self._rtt_thread_halt_evt = None
        self._rtt_check_evt = None

    def _send_rtt_check_evt(self):
        self._rtt_check_evt.set()
